// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.


(LOOP)	
	@offset
	M=-1

	@KBD
	D=M

	@CLEAR
	D;JEQ
	
(FILL)
	@pixel
	M=-1
	
	@CALCULATE_ADDRESS
	0;JMP

(CLEAR)
	@pixel
	M=0

(CALCULATE_ADDRESS)
	@SCREEN
	D=A
	
	@offset
	M=M+1
	
	D=D+M
	
	@current_address
	M=D
	
(DRAW)
	@pixel
	D=M
	
	@current_address
	A=M
	M=D

	@offset
	D=M
	
	@8190
	D=D-A
	
	@DRAW
	D;JLE
	
	@LOOP
	0;JMP